const canvas = document.querySelector('#draw');
const ctx = canvas.getContext('2d');

ctx.strokeStyle = '#BADA55';
ctx.lineJoin = 'round';
ctx.lineCap = 'round';
//init 工具
var isEraser = false, isfur = false, isPencil = false, isRainbow = false, isRainbow2 = false, isText = false, isBubble = false;
var isCircle = false, isRec = false, isTri = false;
var isRedo = isUndo = isClear = isDownload = isUpload = false;


// 切换工具
function changeTools(tname) {
    isfur = isBubble = isEraser = isPencil = isRainbow = isRainbow2 = isText = isCircle = isRec = isTri = isRedo = isUndo = isClear = isDownload = isUpload = false;
    forundo();
    ctx.globalCompositeOperation = "source-over";
    if (tname == 'eraser') {
        changeWidth();
        isEraser = true;
        ctx.globalCompositeOperation = "destination-out";
        canvas.style.cursor="url(./photos/eraser_cur.png),auto";
    } else if (tname == 'pencil') {
        changeWidth();
        isPencil = true;
        canvas.style.cursor="url(./photos/pencil_cur.png),auto";
    } else if (tname == 'rainbow') {
        changeWidth();
        isRainbow = true;
        canvas.style.cursor="url(./photos/pencil_cur.png),auto";
    } else if (tname == 'rainbow2') {
        changeWidth();
        isRainbow2 = true;
        canvas.style.cursor="url(./photos/cat.png),auto";
    } else if (tname == 'bubble') {
        isBubble = true;
        canvas.style.cursor="url(./photos/pencil_cur.png),auto";
    } else if (tname == 'fur') {
        isfur = true;
        canvas.style.cursor="url(./photos/pencil_cur.png),auto";
    } else if (tname == 'text') {
        isText = true;
        canvas.style.cursor="text";
    } else if (tname == 'circle') {
        changeWidth();
        isCircle = true;
        canvas.style.cursor="crosshair";
    } else if (tname == 'rec') {
        changeWidth();
        isRec = true;
        canvas.style.cursor="url(./photos/rec_cur.png),auto";
    } else if (tname == 'tri') {
        changeWidth();
        isTri = true;
        canvas.style.cursor="url(./photos/tri_cur.png),auto";
    } else if (tname == 'redo'){
        isRedo = true;
        redo();
        canvas.style.cursor="context-menu";
    } else if (tname == 'undo'){
        isUndo = true;
        undo();
        canvas.style.cursor="context-menu";
    } else if (tname == 'clear'){
        isClear = true;
        canvas.style.cursor="context-menu";
    } else if (tname == 'down'){
        isDownload = true;
        canvas.style.cursor="context-menu";
    } else if (tname == 'up'){
        isUpload = true;
        canvas.style.cursor="context-menu";
    } else if (tname == 'gray'){
        // 先清空在畫圖
        let canvaspic = new Image(); //建立新的 Image
        canvaspic.src = restoreURL; //載入剛剛存放的影像
        canvaspic.onload = function() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.filter = "grayscale(80%)";            // use CSS filter here :grayscale(80%);            
            ctx.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
            ctx.filter = "none";                   // reset filter
            push();
        }
        canvas.style.cursor="context-menu";
    }
}

//init
let isDrawing = false;
let lastX = 0;
let lastY = 0;
let hue = 0;  //色相，紅色為0度（360度）；黃色為60度....每360度就會有一次循環
let direction = true;  //確認現在是要讓畫筆粗細增或減
let recx = 0, recy = 0;
var oldbub = newbub = 4;

//get color
var color = document.getElementById("color");


//pencil
var flag = false;
function draw(e) {
    if (!isDrawing) return; // stop the fn from running when they are not moused down
	flag = true;
    //指定繪出的線顏色                     
    if(isRainbow == true){
        ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    } else ctx.strokeStyle = ctx.fillStyle = color.value;
	
    //比或彩虹筆
    if(isPencil || isRainbow || isEraser || isfur){
        ctx.beginPath();
        // start from
        ctx.moveTo(lastX, lastY);
        // go to
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
        [lastX, lastY] = [e.offsetX, e.offsetY];
        if(isfur){
            //控制畫筆粗細介於 1~100之間
            if (ctx.lineWidth >= 50 || ctx.lineWidth <= 1) {
                direction = !direction;
            }
            if(direction) {
                ctx.lineWidth++;
            } else {
                ctx.lineWidth--;
            }
        }
        //改色相，每次++，360後歸零
        hue+=3;
        if (hue >= 360) {
            hue = 0;
        }
    }
    else if(isRainbow2){
        let offset = ctx.lineWidth;
        //red
        ctx.strokeStyle = "#ff0000";
        ctx.beginPath();
        ctx.moveTo(lastX , lastY - 2*offset);
        ctx.lineTo(e.offsetX , e.offsetY - 2*offset);
        ctx.stroke();
        //yellow
        ctx.strokeStyle = "#ffc400";
        ctx.beginPath();
        ctx.moveTo(lastX , lastY - offset);
        ctx.lineTo(e.offsetX , e.offsetY - offset);
        ctx.stroke();
        //green
        ctx.strokeStyle = "#9dff00";
        ctx.beginPath();
        ctx.moveTo(lastX , lastY);
        ctx.lineTo(e.offsetX , e.offsetY);
        ctx.stroke();
        //blue
        ctx.strokeStyle = "#008cff";
        ctx.beginPath();
        ctx.moveTo(lastX , lastY + offset);
        ctx.lineTo(e.offsetX , e.offsetY + offset);
        ctx.stroke();
        //purple
        ctx.strokeStyle = "#d400ff";
        ctx.beginPath();
        ctx.moveTo(lastX , lastY + 2*offset);
        ctx.lineTo(e.offsetX , e.offsetY + 2*offset);
        ctx.stroke();

        [lastX, lastY] = [e.offsetX, e.offsetY];
    }
    else if(isBubble){
        var a = lastX-recx;
        var b = lastY-recy;
        var result = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        //畫圓
        if(result >= oldbub+newbub){
            ctx.beginPath();
            ctx.arc(lastX, lastY, newbub, 0, 2 * Math.PI);
            ctx.fill();
            oldbub = newbub;
            newbub = Math.floor(Math.random() * 10) + 5; //5~10
            [recx, recy] = [lastX, lastY];
        }
        [lastX, lastY] = [e.offsetX, e.offsetY];
    }
    else if(isRec){
        // 先清空在畫圖
        let canvaspic = new Image(); //建立新的 Image
        canvaspic.src = restoreURL; //載入剛剛存放的影像
        canvaspic.onload = function() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
            ctx.strokeRect(recx, recy, lastX-recx, lastY-recy);
            [lastX, lastY] = [e.offsetX, e.offsetY];
        }
    }
    else if(isCircle){
        //算兩點距離
        var x = (lastX+recx)/2;
        var y = (lastY+recy)/2;
        var a = lastX-recx;
        var b = lastY-recy;
        var result = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        // 先清空在畫圖
        let canvaspic = new Image(); //建立新的 Image
        canvaspic.src = restoreURL; //載入剛剛存放的影像
        canvaspic.onload = function() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
            //畫圓
            ctx.beginPath();
            ctx.arc(x, y, result/2, 0, 2 * Math.PI);
            ctx.stroke();
            [lastX, lastY] = [e.offsetX, e.offsetY];
        }
    }
    else if(isTri){
        var a = (lastX+recx)/2;
        // 先清空在畫圖
        let canvaspic = new Image(); //建立新的 Image
        canvaspic.src = restoreURL; //載入剛剛存放的影像
        canvaspic.onload = function() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
            //畫三角形
            ctx.beginPath();
            ctx.moveTo(lastX, lastY);
            ctx.lineTo(recx, lastY);
            ctx.lineTo(a, recy);
            ctx.lineTo(lastX, lastY);
            ctx.stroke();
            [lastX, lastY] = [e.offsetX, e.offsetY];
        }
    }
}
//畫圓、長方形、三角形的準備
let restoreURL = 0;
function forundo(){
    restoreURL = canvas.toDataURL();
}
function restore(e){
    /*
    let canvaspic = new Image(); //建立新的 Image
    canvaspic.src = restoreURL; //載入剛剛存放的影像
    return new Promise((resolve, reject) =>{
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        resolve(ctx.drawImage(canvaspic, 0, 0)) //匯出影像並從座標 x:0 y:0 開始
    }); 
    */
    let canvaspic = new Image(); //建立新的 Image
    canvaspic.src = restoreURL; //載入剛剛存放的影像
    canvaspic.onload = function() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
        console.log("!!!!!!!!!");
        ctx.strokeRect(recx, recy, lastX-recx, lastY-recy);
        [lastX, lastY] = [e.offsetX, e.offsetY];
        console.log("after!!!!!!");
    }

}

//canvas滑鼠事件
canvas.addEventListener('mousedown', (e) => {
	isDrawing = true;
	[lastX, lastY] = [e.offsetX, e.offsetY];
    [recx, recy] = [lastX, lastY];
    if(isText){
        console.log("mouse in!!!")
        if(ing){texting();}
        myFunction();
    }
    forundo();
    [textX, textY] = [lastX, lastY];
});
canvas.addEventListener('mousemove', draw);
canvas.addEventListener('mouseup', (e) => {
    if(isfur || isPencil || isEraser || isRainbow || isRec || isCircle || isTri || isBubble){
        if(flag){
            console.log("mouseup, go push");
            push();
            flag = false;
        }
        
    }
    isDrawing = false;
});
canvas.addEventListener('mouseout', () => {
    isDrawing = false;
});
//window滑鼠事件
var checkx, checky;
window.addEventListener('mousedown', (e) =>{
    [checkx, checky] = [e.screenX, e.screenY];
    console.log("mouse = "+ checkx+", " + checky);
    
    if(ing){
        var w = window.innerWidth/2 - 400;
        if(w > checkx || checkx > w+800 || 185 > checky || checky > 585){
            texting();
        }
    }
});
window.addEventListener('load', firstpush)
window.addEventListener('resize', firstpush)
function firstpush() {
    push();
}

//text editor
var textResult;
var ing = false;
var textX, textY;
function myFunction() {
    ing = true;
    var editor = document.createElement("input");
    editor.setAttribute("type", "text");
    editor.setAttribute("id", "edi");
    editor.setAttribute("ref", "test1")
    //editor.onfocus = editor.setAttribute("autofocus", "focus");
    editor.style.position = "fixed";
    var w = window.innerWidth/2 - 400 + lastX ;
    var h = 74 + lastY;

    editor.style.left = w + 'px';
    editor.style.top = h + 'px';
    
    document.body.appendChild(editor);
    var content = document.getElementById("edi");
    content.focus();
    content.addEventListener('keydown', (e) => {
        if(e.key == "Enter"){
            texting();
        }
    });

}

function texting(){
    ctx.globalCompositeOperation = "source-over";
    var content = document.getElementById("edi");
    textResult = content.value;
    content.remove();
            
    var str1 = document.getElementById("fontface").value;
    var str2 = document.getElementById("fontfacesize").value;
    var str3 = str2+str1;

    ctx.font = str3;
    var fontcolor = document.getElementById("color");
    ctx.fillStyle = fontcolor.value;
    ctx.fillText(textResult, textX, textY);
    ing = false;
    if(textResult == ""){return;}
    push();
}

//存放步驟結束當下的陣列
let step = -1;
let userhistory = [];
function push() {
    step++;
    
    userhistory = userhistory.slice(0, step);
    
    userhistory.push(canvas.toDataURL()); //當前影像存成 Base64 編碼的字串並放入陣列
    console.log("now pic is in (step, length) = " + step + ", " + userhistory.length);
}
//儲存畫布的function
function download(position) {
    const dataURL = canvas.toDataURL('image/png') //把影像轉成指定格式的 URL 字串
    position.href = dataURL;
}
//上一步的function
function undo() {
    if (step > 0) {
        step--;
        let canvaspic = new Image(); //建立新的 Image
        canvaspic.src = userhistory[step]; //載入剛剛存放的影像
        canvaspic.onload = function() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
        }
        console.log("undo 成 (step, length) is " + step + ", " + userhistory.length);
    }
}

//下一步的function
function redo() {
    if (step < userhistory.length - 1) {
        step++;
        const canvaspic = new Image(); //建立新的 Image
        canvaspic.src = userhistory[step] //載入剛剛存放的影像
        canvaspic.onload = function() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvaspic, 0, 0) //匯出影像並從座標 x:0 y:0 開始
        }
        
    }
}

//清空畫布
function refresh(){
    step = -1;
    userhistory = [];
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    push();
}



// 改變筆畫、橡皮擦大小
function changeWidth() {
    let w = document.getElementById('rangeValue').value = document.getElementById('rangeIpt').value
    ctx.lineWidth = w
}

//下載
function download(){
    const a = document.createElement("a");

    document.body.appendChild(a);
    a.href = canvas.toDataURL("image/png", 0.1);
    a.download = "canvas-image.png";
    a.click();
    document.body.removeChild(a);
}

//上傳
var fileSelect = document.getElementById("fileSelect"),
    fileElem = document.getElementById("uploader");

fileSelect.addEventListener("click", function (e) { //用來代替input預設外觀
  if (fileElem) {
    fileElem.click();
  }
  e.preventDefault(); // prevent navigation to "#"
}, false);

const reader = new FileReader();
const img = new Image();
const uploadImage = (e) => {
    changeTools('up');
    reader.onload = () => {
    	img.onload = () => {
     		img.width = canvas.width;
            img.height  = canvas.height;
            ctx.clearRect(0,0,canvas.width,canvas.height);
            ctx.drawImage(img, 0, 0);
            console.log("upload的draw檢查，應該要在push前面出現");
            push();forundo();
        };
        img.src = reader.result;
    };
    reader.readAsDataURL(e.target.files[0]);
};
const imageLoader = document.getElementById('uploader');
imageLoader.addEventListener("change", uploadImage);

# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| 彩虹筆(會一直換色)                                            | 1~5%     | Y         |
| 彩虹筆2(畫出來是彩虹)                                            | 1~5%     | Y         |
| 泡泡筆                                  | 1~5%     | Y         |
| 偽毛筆                                  | 1~5%     | Y         |
| 灰階濾鏡                                 | 1~5%     | Y         |
---

### How to use 

1. ![](https://i.imgur.com/FWnL82S.png)這個是普通筆
2. ![](https://i.imgur.com/AZxa4lT.jpg)可以調筆/橡皮擦的大小，只能用滑輪調，旁邊的text我設定成沒辦法輸入，純顯示數字
3. ![](https://i.imgur.com/HoyMPvg.jpg)選顏色的，按一下後便變![](https://i.imgur.com/uJJCEhg.jpg)


4. ![](https://i.imgur.com/SFBDRjF.png)這個是橡皮擦
5. ![](https://i.imgur.com/X3wxcAO.png)這是文字框，如果打字打一半，去按到其他地方，會直接完成打字。字的顏色跟畫筆顏色一樣可改
6. ![](https://i.imgur.com/wJW6T45.jpg)換字型。![](https://i.imgur.com/VojnPpw.jpg)換字型大小。

7. ![](https://i.imgur.com/elmU2ze.png)畫圈(空心)，按住時隨滑鼠改大小，放開就畫上去
8. ![](https://i.imgur.com/Ao5hrIv.png)畫方形(空心)
9. ![](https://i.imgur.com/pz4Pdui.png)畫三角形(空心)
10. ![](https://i.imgur.com/lbNY2Kn.png)按一下就redo
11. ![](https://i.imgur.com/YDc41d9.png)按一下就undo
12. ![](https://i.imgur.com/VdySL1H.png)按一下就清空
13. ![](https://i.imgur.com/vuaDNC6.png)按一下儲存現在的圖
14. ![](https://i.imgur.com/p2qT7S1.png)按一下選擇上傳的圖

### Function description

1. ![](https://i.imgur.com/5rMvzWH.png)這個是彩虹筆，跟普通筆用法一樣，一樣可以改粗細，畫的顏色則是隨彩虹漸變
2. ![](https://i.imgur.com/iQDQGCw.png)這個是泡泡筆，跟普通筆用法一樣，只是畫出來是分開的大小不一的圓，圓大小隨機。
3. ![](https://i.imgur.com/HicPTf1.png)這個是偽毛筆，跟普通筆用法一樣，只是粗細大小隨機，拿來寫字會很像毛筆
4. ![](https://i.imgur.com/ThrK0Ym.png)這個是灰階濾鏡，按一下變灰80%，多按就更灰
5. ![](https://i.imgur.com/oUiL783.png)這個是彩虹筆2，跟普通筆用法一樣，一樣可以改粗細，畫出來就是彩虹

### Gitlab page link

    https://108062312.gitlab.io/AS_01_WebCanvas/




